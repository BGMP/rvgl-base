#!/bin/bash
# Initial bootstrapping.
# File inception by Huki (2015-05-01).

while [ $# -gt 0 ]; do
  case "$1" in
    --data-dir=*) data="${1#*=}";;
  esac
  shift
done

# Create data folder
if [ -n "$data" ]; then
  ln -s "$data" data
else
  mkdir -p data
fi

# Create distrib folders
(cd distrib && ./init.sh)
