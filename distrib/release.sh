#!/bin/bash
# Create releases for distribution.
# File inception by Huki (2015-05-01).

for i in "$@"; do
  case $i in
    --clean) clean=true;;
    --tag) tag=true;;
  esac
done

# Get version
DATE=$(date +"%y.%m%d")
SUFFIX="a"

BUILD=$(cd ../devel && ./build.sh && ./bin/version)
if [ -z "${BUILD}" ]; then
  BUILD="${DATE}${SUFFIX}"
fi

# Clean folders
if [ "$clean" = true ]; then
  rm -rf ./RVGL
  rm -rf ./bin
  rm -f *.exe
  rm -f *.apk
  rm -f *.dmg
  rm -f *.7z
fi

# Create folders
./init.sh

# Tag repositories
if [ "$tag" = true ]; then
  (cd .. && git submodule foreach git tag -a ${BUILD} -m "version ${BUILD}")
  (cd .. && git tag -a ${BUILD} -m "version ${BUILD}")
  (cd .. && git submodule foreach git push -u origin ${BUILD})
  (cd .. && git push -u origin ${BUILD})
fi

# Exit early?
if [ "$clean" = true ] || [ "$tag" = true ]; then
  exit
fi

# Extract macOS binaries
package="rvgl_macos.dmg"
base="RVGL/RVGL.app/Contents"
paths=(
  "$base/Frameworks"
  "$base/MacOS"
  "$base/Resources/rvgl.icns"
  "$base/Info.plist"
)

if [ -f ./$package ]; then
  rm -rf ./RVGL
  rm -rf ./bin/macos/*

  7z x ./$package "${paths[@]}"
  chmod -R ugo+x "${paths[@]::2}"

  mv ./$base/* ./bin/macos
  rm -rf ./RVGL
fi

# Copy binaries
cp -r ./bin/* ./platform

# Build Windows setup
(cd ../devel/windows/setup && ./build.sh --win32)
(cd ../devel/windows/setup && ./build.sh --win64)

# Linux release
filename="rvgl_${BUILD}_linux.7z"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./assets && 7z a ../$filename ./* -mx=9 -ms)
(cd ./platform/linux && 7z a ../../$filename ./* -mx=9 -ms)

# Win32 release
filename="rvgl_${BUILD}_win32.7z"
setup="rvgl_setup_win32.exe"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./assets && 7z a ../$filename ./* -mx=9 -ms)
(cd ./platform/win32 && 7z a ../../$filename ./* -mx=9 -ms)

if [ -f ./$setup ]; then
  package="$filename"
  filename="rvgl_${BUILD}_setup_win32.exe"

  if [ -f ./$filename ]; then
    rm -f ./$filename
  fi

  cat ./$setup ./$package > ./$filename
  rm -f ./$setup
fi

# Win64 release
filename="rvgl_${BUILD}_win64.7z"
setup="rvgl_setup_win64.exe"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./assets && 7z a ../$filename ./* -mx=9 -ms)
(cd ./platform/win64 && 7z a ../../$filename ./* -mx=9 -ms)

if [ -f ./$setup ]; then
  package="$filename"
  filename="rvgl_${BUILD}_setup_win64.exe"

  if [ -f ./$filename ]; then
    rm -f ./$filename
  fi

  cat ./$setup ./$package > ./$filename
  rm -f ./$setup
fi

# Dreamcast pack
filename="rvgl_dcpack.7z"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

(cd ./dcpack && 7z a ../$filename ./* -mx=9 -ms)

# Android package
filename="rvgl_${BUILD}_android.apk"
package="rvgl_android.apk"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

if [ -f ./$package ]; then
  cp ./$package $filename
  rm -f ./$package
fi

# macOS package
filename="rvgl_${BUILD}_macos.dmg"
package="rvgl_macos.dmg"

if [ -f ./$filename ]; then
  rm -f ./$filename
fi

if [ -f ./$package ]; then
  cp ./$package $filename
  rm -f ./$package
fi
